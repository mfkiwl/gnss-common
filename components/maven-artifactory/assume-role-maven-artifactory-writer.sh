#!/usr/bin/env bash

# This script assumes role gnss-maven-artifactory-$env-writer

set -euo pipefail

function usage {
    cat << 'EOF'
Usage: eval $(assume-role-maven-artifactory-writer.sh <environment>)
where
    environment is dev, test, or prod
EOF
}

while [[ $# -gt 0 ]]; do
    case $1 in
       dev|test|prod)
            env=$1
            shift
            ;;
        *)
            echo "Unknown option: $1"
            usage
            exit 1
            ;;
    esac
done

if [[ -z ${env+x} ]]; then
    echo "Unspecified environment"
    usage
    exit 1
fi

export TF_DATA_DIR
TF_DATA_DIR=$(mktemp -d)
trap 'rm -rf "$TF_DATA_DIR"' EXIT

scriptDir="$(dirname "${BASH_SOURCE[0]}")"
cd "$scriptDir"

terraform init -backend-config "./environments/$env/backend.cfg" > /dev/null
terraform workspace select "$env" > /dev/null || $TERRAFORM_BIN workspace new "$env" > /dev/null
writer_role_arn=$(terraform output writer_role_arn)

credentials=$(aws sts assume-role --role-arn "$writer_role_arn" --role-session-name maven-artifactory | jq '.Credentials')

echo "export AWS_ACCESS_KEY_ID=$(jq <<< "$credentials" '.AccessKeyId' -r)"
echo "export AWS_SECRET_ACCESS_KEY=$(jq <<< "$credentials" '.SecretAccessKey' -r)"
echo "export AWS_SESSION_TOKEN=$(jq <<< "$credentials" '.SessionToken' -r)"
