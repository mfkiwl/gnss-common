variable "region" {
}

variable "accounts_iam_root_arns" {
  type = list(string)
}

variable "owner" {
  default = "gnss-informatics"
}

variable "system" {
  default = "gnss-maven-artifactory"
}
