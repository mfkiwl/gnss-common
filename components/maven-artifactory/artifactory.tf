terraform {
  required_version = ">= 0.12.0"

  backend "s3" {
    encrypt = true
  }
}

locals {
  prefix = "${var.system}-${terraform.workspace}"
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

data "aws_caller_identity" "current" {}

resource "aws_s3_bucket" "artifactory" {
  bucket = local.prefix
  acl    = "public-read"
}

resource "aws_iam_policy" "write" {
  name = "${local.prefix}-write"
  path = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.artifactory.arn}/*"
      ]
    }
  ]
}
EOF
}

module "cross_account_role" {
  source = "git@github.com:infrablocks/terraform-aws-cross-account-role.git?ref=0.3.0"

  role_name  = "${local.prefix}-writer"
  policy_arn = aws_iam_policy.write.arn

  assumable_by_account_ids = var.accounts_iam_root_arns
}

output "writer_role_arn" {
  value = module.cross_account_role.role_arn
}
