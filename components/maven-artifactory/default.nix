{ stdenv, lib, awscli2, bash, coreutils, git, jq, openssh, makeWrapper, terraform_0_12 }:

stdenv.mkDerivation rec {
  name = "assume-role-maven-artifactory-writer";
  src = ./.;

  nativeBuildInputs = [
    makeWrapper
  ];

  phases = "installPhase";

  installPhase = ''
    mkdir -p $out/{bin,dist/${name}}

    cp -r $src/* $out/dist/${name}
    chmod +x $out/dist/${name}/${name}.sh

    wrapProgram $out/dist/${name}/${name}.sh \
      --set PATH ${lib.makeBinPath [ awscli2 bash coreutils git jq openssh terraform_0_12 ]}

    ln -s $out/dist/${name}/${name}.sh $out/bin/${name}.sh
  '';
}
