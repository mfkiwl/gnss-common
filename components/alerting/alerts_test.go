package alerting_test

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"regexp"
	"testing"

	"bitbucket.org/geoscienceaustralia/gnss-common/components/alerting"
	"github.com/gorilla/feeds"
)

var (
	alertsJson []byte = []byte(`{
		"status": true,
		"alerts": [
			{
				"id": "ID1",
				"monitor_id": "MonitorID1",
				"schema_version": 1,
				"monitor_version": 2,
				"monitor_name": "SITE00FOO - Stream",
				"trigger_id": "TriggerID1",
				"trigger_name": "Completeness < 95%",
				"state": "ACTIVE",
				"error_message": null,
				"alert_history": [],
				"severity": "1",
				"action_execution_results": [],
				"start_time": 1585981907230,
				"last_notification_time": 1585984607275,
				"end_time": null,
				"acknowledged_time": null,
				"version": 42
			},
			{
				"id": "ID2",
				"monitor_id": "MonitorID2",
				"schema_version": 1,
				"monitor_version": 2,
				"monitor_name": "SITE00BAR - Stream",
				"trigger_id": "TriggerID2",
				"trigger_name": "Latency > 2 Seconds",
				"state": "COMPLETED",
				"error_message": null,
				"alert_history": [],
				"severity": "1",
				"action_execution_results": [],
				"start_time": 1585975493354,
				"last_notification_time": 1585978973207,
				"end_time": 1585979033238,
				"acknowledged_time": null,
				"version": 69
			}
		],
		"totalAlerts": 2
	}`)

	alertsObject alerting.Alerts = alerting.Alerts{
		Status: true,
		Alerts: []alerting.Alert{
			alerting.Alert{
				ID:                   "ID1",
				MonitorID:            "MonitorID1",
				SchemaVersion:        1,
				MonitorVersion:       2,
				MonitorName:          "SITE00FOO - Stream",
				TriggerID:            "TriggerID1",
				TriggerName:          "Completeness < 95%",
				State:                "ACTIVE",
				AlertHistory:         []string{},
				Severity:             "1",
				StartTime:            1585981907230,
				LastNotificationTime: 1585984607275,
				Version:              42,
			},
			alerting.Alert{
				ID:                   "ID2",
				MonitorID:            "MonitorID2",
				SchemaVersion:        1,
				MonitorVersion:       2,
				MonitorName:          "SITE00BAR - Stream",
				TriggerID:            "TriggerID2",
				TriggerName:          "Latency > 2 Seconds",
				State:                "COMPLETED",
				AlertHistory:         []string{},
				Severity:             "1",
				StartTime:            1585975493354,
				LastNotificationTime: 1585978973207,
				EndTime:              1585979033238,
				Version:              69,
			},
		},
		TotalAlerts: 2,
	}

	feedObject feeds.Feed = feeds.Feed{
		Title:       "Geoscience Australia GNSS Alerts",
		Link:        &feeds.Link{Href: "https://gnss.ga.gov.au"},
		Description: "",
		Author:      &feeds.Author{Name: "GA", Email: "gnss@ga.gov.au"},
		Items:       alertsObject.AsFeedItems(),
	}

	atomString string = `<?xml version="1.0" encoding="UTF-8"?><feed xmlns="http://www.w3.org/2005/Atom">
  <title>Geoscience Australia GNSS Alerts</title>
  <id>https://gnss.ga.gov.au</id>
  <updated></updated>
  <link href="https://gnss.ga.gov.au"></link>
  <author>
    <name>GA</name>
    <email>gnss@ga.gov.au</email>
  </author>
  <entry>
    <title>SITE00FOO - Stream Completeness &lt; 95%</title>
    <updated>2020-04-04T07:16:47Z</updated>
    <id>ID1</id>
    <link href="http://gnss.ga.gov.au/" rel="alternate"></link>
    <summary type="html">ACTIVE,2020-04-04T06:31:47Z,</summary>
  </entry>
  <entry>
    <title>SITE00BAR - Stream Latency &gt; 2 Seconds</title>
    <updated>2020-04-04T05:42:53Z</updated>
    <id>ID2</id>
    <link href="http://gnss.ga.gov.au/" rel="alternate"></link>
    <summary type="html">COMPLETED,2020-04-04T04:44:53Z,</summary>
  </entry>
</feed>`
)

func TestAlertsMarshalling(t *testing.T) {
	var alerts alerting.Alerts

	if err := json.Unmarshal(alertsJson, &alerts); err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(alerts, alertsObject) {
		t.Error("unmarshalled json string does not equal struct")
	}
}

func TestAlertsAsFeedItems(t *testing.T) {
	var alertsItems []*feeds.Item = alertsObject.AsFeedItems()

	feed := feedObject
	feed.Items = alertsItems

	atom, err := feed.ToAtom()
	if err != nil {
		t.Error(err)
	}

	if atom != atomString {
		t.Error("alerts.AsFeedItems returned incorrect atom string")
	}
}

func TestGetAlerts(t *testing.T) {
	mockServer := httptest.NewTLSServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, string(alertsJson))
		}))

	defer mockServer.Close()

	// Overwrite HTTP Client to ignore TLS
	alerting.HTTPClient = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	alerts, err := alerting.GetAlerts(mockServer.Listener.Addr().String())
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(alerts, alertsObject) {
		t.Error("mock server response does not match alerts")
	}
}

func TestGenerateAtomFeed(t *testing.T) {
	atom, err := alerting.GenerateAtomFeed(alertsObject)
	if err != nil {
		t.Error(err)
	}

	// GenerateFeed uses time.now to populate the updated element, remove that line
	updatePattern := regexp.MustCompile("\n  <updated>.*</updated>")
	if updatePattern.ReplaceAllString(atom, "") != updatePattern.ReplaceAllString(atomString, "") {
		t.Error("alerting.GenerateFeed returned incorrect atom string")
	}
}
