module bitbucket.org/geoscienceaustralia/gnss-common/components/alerting

go 1.14

require (
	github.com/gorilla/feeds v1.1.1
	github.com/kr/pretty v0.2.0 // indirect
)
