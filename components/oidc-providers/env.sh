#!/usr/bin/env bash

terraform() {
    terraform-0.13 $@
}

TF_VAR_gnss_common_reader_role_dev_arn=$(gnss-common-reader-role.sh -e dev)
export TF_VAR_gnss_common_reader_role_dev_arn

TF_VAR_gnss_common_reader_role_test_arn=$(gnss-common-reader-role.sh -e test)
export TF_VAR_gnss_common_reader_role_test_arn

TF_VAR_gnss_common_reader_role_prod_arn=$(gnss-common-reader-role.sh -e prod)
export TF_VAR_gnss_common_reader_role_prod_arn

init-kubeconfig.sh dev
context=$(kubectl config current-context)
export TF_VAR_gnss_eks_dev_cluster_id=${context#*/}

init-kubeconfig.sh test
context=$(kubectl config current-context)
export TF_VAR_gnss_eks_test_cluster_id=${context#*/}

init-kubeconfig.sh prod
context=$(kubectl config current-context)
export TF_VAR_gnss_eks_prod_cluster_id=${context#*/}
