# Cognito insists that the SES email identity is verified in us-east-1
provider "aws" {
  alias       = "us_east_1"
  region      = "us-east-1"
  max_retries = 10
}

# TODO: Consider whether this should be a noreply address
# TODO: Consider whether this should be moved to a separate component,
# as many common components may need to use it
resource "aws_ses_email_identity" "from_address" {
  provider = aws.us_east_1
  email    = var.from_address
}
