## Cognito Auth

Shared Authentication and Authorization system for GA GNSS applications.

Applications should create their own App Client and Groups in the User Pool.


#### TODO

**Custom Email** - Consider email delivery through SNS for registration emails.
We should also consider customizing the [Verification Message Template.](https://www.terraform.io/docs/providers/aws/r/cognito_user_pool.html#verification-message-template)

**Backup Users** - Changes to the schema will cause the User Pool to be deleted
and recreated. Deletion protection is enabled on the Terraform resource, but in
the event of a legitimate need to recreate the pool (or a major accident) we
should backup users. Custom attributes can be added without recreating the user
pool, but they cannot be deleted (so choose new attributes wisely).

**Customize Registration Page** - It is not possible for the Cognito Hosted UI
to inclue custom attributes. We would like to add an "industry" attribute as
the relevant information we need for reporting about our user base. This will
likely involve writing our own UI for sign-in / sign-up using something like
[Amplify for Angular](https://aws-amplify.github.io/docs/js/angular). It is 
possible to use [AWS CLI](https://docs.aws.amazon.com/cli/latest/reference/cognito-idp/admin-update-user-attributes.html)
to update or add new user attributes after sign-up.
