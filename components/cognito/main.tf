terraform {
  required_version = "= 0.12.29"

  backend "s3" {
    encrypt = true
  }
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

resource "aws_cognito_user_pool" "users" {
  name                     = "gnss-users-${terraform.workspace}"
  alias_attributes         = ["email"]
  auto_verified_attributes = ["email"]

  lifecycle {
    prevent_destroy = true
  }

  username_configuration {
    case_sensitive = false
  }

  schema {
    name                     = "email"
    developer_only_attribute = false
    attribute_data_type      = "String"
    mutable                  = false
    required                 = true

    string_attribute_constraints {
      max_length = "2048"
      min_length = "0"
    }
  }

  email_configuration {
    email_sending_account = "DEVELOPER"
    source_arn            = aws_ses_email_identity.from_address.arn
  }

  verification_message_template {
    default_email_option  = "CONFIRM_WITH_LINK"
    email_message_by_link = "Confirm your email address: {##Click Here##}"
    email_subject_by_link = "Geoscience Australia GNSS Account Confirmation ${terraform.workspace == "prod" ? "" : terraform.workspace}"
  }
}

# TODO: Add UI customizations to cognito login screen when added to terraform
# https://github.com/terraform-providers/terraform-provider-aws/issues/3634

resource "aws_cognito_user_pool_domain" "users" {
  domain       = "gnss-users-${terraform.workspace}"
  user_pool_id = aws_cognito_user_pool.users.id
}

resource "aws_cognito_user_group" "superuser" {
  name         = "superuser"
  user_pool_id = aws_cognito_user_pool.users.id
  description  = "System Admin Users"
}
