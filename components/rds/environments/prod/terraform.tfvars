# General config

cluster_name = "gnss-rds-prod"

# We are not able to use the cluster_name var as only
# alpha-numeric chars are allowed for the RDS DBname
db_name = "gnssRdsDev"

create_db_instance = false
