variable "region" {
  description = "The AWS region we are going to create these resources in"
}

variable "accounts_iam_root_arns" {
  type = list(string)
}

variable "cluster_name" {
  description = "A unique name for this stack that will be used to name shared resources (e.g. vpc)"
}

variable "owner" {
  default     = "gnss-informatics"
  description = "Identifies who is responsible for these resources"
}

# VPC & subnets
# ===========
variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "database_subnet_cidrs" {
  description = "List of private database cidrs, for all available availability zones."
  type        = list(string)
  default     = ["10.0.21.0/24", "10.0.22.0/24", "10.0.23.0/24"]
}

# RDS
# ===========
variable "create_db_instance" {
  type    = bool
  default = true
}

variable "db_engine" {
  type    = string
  default = "postgres"
}

variable "db_engine_version" {
  type    = string
  default = "11.6"
}

variable "db_instance_type" {
  type    = string
  default = "db.t2.medium"
}

variable "db_name" {}
