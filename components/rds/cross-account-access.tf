module "vpc_peering_requester_role" {
  source = "git@github.com:JamesWoolfenden/terraform-aws-cross-account-role?ref=v0.2.12"

  role_name = "${var.cluster_name}-vpc-peering-requester"

  policy_arns = [
    "arn:aws:iam::aws:policy/AmazonVPCReadOnlyAccess",
    aws_iam_policy.accept_vpc_peering.arn,
  ]

  principal_arns = var.accounts_iam_root_arns

  common_tags = {
    name = "${var.cluster_name}-vpc-peering-requester"
  }
}

resource "aws_iam_policy" "accept_vpc_peering" {
  name = "${var.cluster_name}-accept-vpc-peering"
  path = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:AcceptVpcPeeringConnection",
        "ec2:CreateTags",
        "ec2:CreateRoute",
        "ec2:DeleteRoute",
        "rds:DescribeDBInstances",
        "rds:ListTagsForResource"
      ],
      "Effect": "Allow",
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

