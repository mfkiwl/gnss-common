resource "aws_ssm_parameter" "rds_master_password" {
  name  = "/${var.cluster_name}/user/master/password"
  type  = "SecureString"
  value = data.external.rds_master_credentials.result.password
}

data "external" "rds_master_credentials" {
  program = ["bash", "${path.module}/generate-rds-credentials.sh"]
}

