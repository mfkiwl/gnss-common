terraform {
  required_version = "= 0.12.29"

  backend "s3" {
    encrypt = true
  }
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source  = "git@github.com:terraform-aws-modules/terraform-aws-vpc.git?ref=v2.70.0"

  name             = var.cluster_name
  cidr             = var.vpc_cidr
  azs              = data.aws_availability_zones.available.names
  database_subnets = var.database_subnet_cidrs

  create_database_subnet_route_table = true

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway           = false
  enable_s3_endpoint           = true

  tags = {
    Workspace  = terraform.workspace
    Owner      = var.owner
    Cluster    = var.cluster_name
    Created_by = "terraform"
  }
}

module "rds_sg" {
  source = "git@github.com:terraform-aws-modules/terraform-aws-security-group//modules/postgresql?ref=v3.17.0"

  name   = "${var.cluster_name}-postgresql"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["10.0.0.0/8"]
}

module "rds" {
  source  = "git@github.com:terraform-aws-modules/terraform-aws-rds.git?ref=v2.20.0"

  identifier = var.cluster_name

  create_db_instance = var.create_db_instance

  engine            = var.db_engine
  engine_version    = var.db_engine_version
  instance_class    = var.db_instance_type
  # PostgreSQL supports provisioned storage from 100 to 32768 GiB
  allocated_storage = 100

  name     = var.db_name
  username = data.external.rds_master_credentials.result.username
  password = aws_ssm_parameter.rds_master_password.value
  port     = "5432"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [module.rds_sg.this_security_group_id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # Creates an IAM role that permits RDS to send enhanced
  # monitoring metrics to CloudWatch
  monitoring_interval = "30"
  monitoring_role_name = var.cluster_name
  create_monitoring_role = true

  tags = {
    Workspace  = terraform.workspace
    Owner      = var.owner
    Cluster    = var.cluster_name
    Created_by = "terraform"
  }

  # DB subnet group
  subnet_ids = module.vpc.database_subnets

  # DB parameter group
  family = "postgres11"

  # DB option group
  major_engine_version = "11"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = var.cluster_name

  deletion_protection = var.create_db_instance
}
