variable "region" {}

variable "resource_name_prefix" {}

variable "elasticsearch_endpoint" {
  default = "https://search-gnss-elasticsearch-prod-5omhch5quzlu5dcpbct4ev5qz4.ap-southeast-2.es.amazonaws.com"
}

variable "elasticsearch_index_name_prefix" {}
