variable "region" {
  description = "The AWS region we are going to create these resources in"
}

variable "accounts_iam_root_arns" {
  type = list(string)
}

variable "iam_oidc_providers" {
  description = "IAM OpenId Connect Provider ARNs"
  type = list(string)
}

variable "system" {
  default = "gnss-elasticsearch"
}
