variable "elasticsearch_endpoint" {
  description = "host:port"
  type = string
}

variable "index_prefix" {
  description = "Elasticsearch index prefix, e.g., 'gnss-data' will generate indices like 'gnss-data-2020-02'"
  type = string
}

variable "namespace" {
  description = <<-EOF
    Fluentd-elasticsearch will be deployed into this namespace and it will watch container logs in
    this namespace.
  EOF

  type = string
}

variable "pod" {
  description = <<-EOF
    Identify containers to watch, default is all containers in all pods in the given namespace.
  EOF
  type = string
  default = null
}

variable "line_pattern" {
  description = <<-EOF
    Log lines to match. Lines not matching this pattern will be appended to the most recently
    matched log lines.

    Line pattern

    ^\[(?<time>[^\]]*)\] \[(?<level>[^\]]*)\] \[(?<logger>[^\]]*)\] (?<message>.*)

    will add 'time', 'level', 'logger', and 'message' fields to the document sent to
    Elasticsearch. Field 'time' is uninterpreted, the document @timestamp field will
    instead be parsed from the docker log event timestamp.
  EOF
  type = string
}

variable "filter" {
  description = <<-EOF
    Optionally apply further filters to fields defined in line_pattern.
    
    Given the line_pattern above, the filter

    filter = {
      fields   = ["level", "logger"],
      patterns = [
        [".*", "WARN|ERROR"],
        ["au\\.gov\\.ga\\.*", "INFO"],
      ]
    }

    Will restrict matched log events to warnings and errors for all loggers
    (including 'au.gov.ga') and info messages only for loggers under 'au.gov.ga'.
  EOF
  type = object({
    fields: list(string),
    patterns: list(list(string)),
  })
  default = {
    fields = []
    patterns = []
  }
}
