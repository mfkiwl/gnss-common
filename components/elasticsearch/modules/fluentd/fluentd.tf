# An installation of Fluentd-elasticsearch to forward kubernetes log events to Elasticsearch.
# See variables.tf for usage.

resource "helm_release" "elasticsearch" {
  namespace  = var.namespace
  name       = "fluentd-elasticsearch"
  repository = "https://kiwigrid.github.io"
  chart      = "fluentd-elasticsearch"
  version    = "9.6.2"

  recreate_pods = true

  values = [
    templatefile("${path.module}/values.yaml", {
      elasticsearch_endpoint = var.elasticsearch_endpoint
      index_prefix           = var.index_prefix
      namespace              = var.namespace
      pod                    = var.pod != null ? var.pod : "*"

      line_pattern    = trimspace(var.line_pattern)
      filter_fields   = join(" ", [ for field in var.filter.fields: "$${record[\"${field}\"]}" ])
      filter_patterns = [ for pattern in var.filter.patterns: "(${join(") (", pattern)})" ]
    })
  ]
}
