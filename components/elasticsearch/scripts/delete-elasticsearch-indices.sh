#!/usr/bin/env bash

# Delete with confirmation all Elasticsearch indices whose ids match regex $index-$date.*

set -euo pipefail

deleteAll=n

while [[ $# -gt 0 ]]; do
    case $1 in
        -i|--index)
            index=$2
            shift 2
            ;;
        -d|--date )
            date=$2
            shift 2
            ;;
        -a )
            deleteAll=y
            shift 1
            ;;
        * )
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
done

# AWS requires that HTTP requests to Elasticsearch be signed.
# The alternative is to allow unauthenticated access, preferably restricted
# by source IP.
awsEsProxyContainerId=$(docker run --rm -d \
    -e "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID" -e "AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY" \
    -p 9200:9200 abutaha/aws-es-proxy:v1.0 \
    -endpoint https://search-gnss-elasticsearch-prod-5omhch5quzlu5dcpbct4ev5qz4.ap-southeast-2.es.amazonaws.com/ \
    -listen 0.0.0.0:9200)

trap 'docker stop $awsEsProxyContainerId > /dev/null' EXIT

elasticsearchUrl="http://localhost:9200"

readarray -t indexIds < <(curl -s "$elasticsearchUrl"/_stats/store | jq -r ".indices | keys | .[] | match(\"$index-$date.*\") | .string")

for indexId in "${indexIds[@]}"; do
    echo "$indexId"
done

if [[ $deleteAll == "y" ]]; then
    read -r -n1 -p "Delete all?"
    deleteAll=$REPLY
    echo
fi

for indexId in "${indexIds[@]}"; do
    if [[ $deleteAll != "y" ]]; then
        read -r -n1 -p "Delete ${indexId}? "
        deleteCurrent=$REPLY
        echo
    fi
    if [[ $deleteAll == "y" || $deleteCurrent = "y" ]]; then
        cmd="curl -XDELETE $elasticsearchUrl/$indexId"
        echo "$cmd"
        $cmd
        echo
    fi
done
