# Domain Policy provides cross account access to Elasticsearch
data "aws_iam_policy_document" "cluster_access" {
  statement {
    actions = ["es:ESHttp*"]

    principals {
      type        = "AWS"
      identifiers = var.accounts_iam_root_arns
    }
  }
}

resource "aws_elasticsearch_domain_policy" "cluster" {
  domain_name     = aws_elasticsearch_domain.cluster.domain_name
  access_policies = data.aws_iam_policy_document.cluster_access.json
}
