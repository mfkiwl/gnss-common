locals {
  namespace = "external-secrets"
}

resource "kubernetes_namespace" "external_secrets" {
  metadata {
    name = local.namespace
  }
}

data "local_file" "docker_config_json" {
  filename = pathexpand("~/.docker/config.json")
}

resource "kubernetes_secret" "docker_credentials" {
  metadata {
    namespace = local.namespace
    name      = "docker-credentials"
  }

  data = {
    ".dockerconfigjson" = data.local_file.docker_config_json.content
  }

  type = "kubernetes.io/dockerconfigjson"
}


resource "helm_release" "external_secrets" {
  namespace  = "external-secrets"
  name       = "ssm-parameters"
  repository = "https://external-secrets.github.io/kubernetes-external-secrets/"
  chart      = "kubernetes-external-secrets"

  set {
    name = "env.AWS_REGION"
    value = var.region
  }

  set {
    name = "podAnnotations.iam\\.amazonaws\\.com/role"
    value = aws_iam_role.parameter_store_read_only.arn
  }

  set {
    name = "imagePullSecrets[0].name"
    value = "docker-credentials"
  }

  depends_on = [kubernetes_secret.docker_credentials]
}
