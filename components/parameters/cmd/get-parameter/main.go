package main

// Assume gnss-common Parameter Store Read Only role and fetch parameter by name

import (
	"encoding/json"
	"flag"
	"fmt"

	"bitbucket.org/geoscienceaustralia/gnss-common/components/parameters"
)

func main() {
	name := flag.String("name", "", "parameter name")
	decrypt := flag.Bool("d", false, "decrypt parameter value")
	flag.Parse()

	reader := parameters.NewDefaultParameterStoreReader()

	if *name == "" {
		params, err := reader.GetParameters()
		if err != nil {
			panic(err)
		}
		jsonBytes, err := json.Marshal(params)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(jsonBytes))
	} else {
		param, err := reader.GetParameter(*name, *decrypt)
		if err != nil {
			panic(err)
		}
		fmt.Println(*param.Parameter.Value)
	}
}
