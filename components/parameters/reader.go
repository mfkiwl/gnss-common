package parameters

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
)

var (
	ReaderRoleARN string = "arn:aws:iam::334594953176:role/parameter-store-prod-read-only"
)

// ParameterStoreReader wraps a Session to assume a Read Only role
// for accessing gnss-common SSM Parameters
type ParameterStoreReader struct {
	Session *session.Session
}

func NewParameterStoreReader(sess *session.Session) ParameterStoreReader {
	return ParameterStoreReader{
		Session: session.Must(session.NewSession(&aws.Config{
			Credentials: stscreds.NewCredentials(sess, ReaderRoleARN),
		})),
	}
}

func NewDefaultParameterStoreReader() (p ParameterStoreReader) {
	return NewParameterStoreReader(session.Must(session.NewSession()))
}

func (p ParameterStoreReader) GetParameter(name string, decrypt bool) (param *ssm.GetParameterOutput, err error) {
	return ssm.New(p.Session).GetParameter(&ssm.GetParameterInput{
		Name:           aws.String(name),
		WithDecryption: aws.Bool(decrypt),
	})
}

func (p ParameterStoreReader) GetParameters() (value *map[string]string, err error) {
	client := ssm.New(p.Session)
	describeOutput, err := client.DescribeParameters(&ssm.DescribeParametersInput{})
	if err != nil {
		return nil, err
	}
	names := make([]*string, len(describeOutput.Parameters))
	for i, param := range describeOutput.Parameters {
		names[i] = param.Name
	}
	paramsOutput, err := client.GetParameters(&ssm.GetParametersInput{
		Names:          names,
		WithDecryption: aws.Bool(true),
	})
	if err != nil {
		return nil, err
	}
	values := make(map[string]string)
	for _, param := range paramsOutput.Parameters {
		values[*param.Name] = *param.Value
	}
	return &values, nil
}
