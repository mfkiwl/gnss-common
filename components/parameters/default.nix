{ buildGoPackage }:

buildGoPackage rec {
  name = "parameters";
  src = ./.;
  goPackagePath = "bitbucket.org/geoscienceaustralia/gnss-common/components/parameters";
  goDeps = ./deps.nix; # deps.nix was generated with vgo2nix
}
