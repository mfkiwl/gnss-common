# file generated from go.mod using vgo2nix (https://github.com/adisbladis/vgo2nix)
[
  {
    goPackagePath = "github.com/aws/aws-sdk-go";
    fetch = {
      type = "git";
      url = "https://github.com/aws/aws-sdk-go";
      rev = "v1.30.12";
      sha256 = "1bp8gsz2bjgw7k2gfwmgpwypq001z3fwrkma93a20ha3b5hzbdks";
    };
  }
  {
    goPackagePath = "github.com/davecgh/go-spew";
    fetch = {
      type = "git";
      url = "https://github.com/davecgh/go-spew";
      rev = "v1.1.0";
      sha256 = "0d4jfmak5p6lb7n2r6yvf5p1zcw0l8j74kn55ghvr7zr7b7axm6c";
    };
  }
  {
    goPackagePath = "github.com/go-sql-driver/mysql";
    fetch = {
      type = "git";
      url = "https://github.com/go-sql-driver/mysql";
      rev = "v1.5.0";
      sha256 = "11x0m9yf3kdnf6981182r824psgxwfaqhn3x3in4yiidp0w0hk3v";
    };
  }
  {
    goPackagePath = "github.com/jmespath/go-jmespath";
    fetch = {
      type = "git";
      url = "https://github.com/jmespath/go-jmespath";
      rev = "v0.3.0";
      sha256 = "12qgp7yb7yfjxhd311kb820fcjmg7gd4hp2fc4v6x8s7121pwnjp";
    };
  }
  {
    goPackagePath = "github.com/pkg/errors";
    fetch = {
      type = "git";
      url = "https://github.com/pkg/errors";
      rev = "v0.9.1";
      sha256 = "1761pybhc2kqr6v5fm8faj08x9bql8427yqg6vnfv6nhrasx1mwq";
    };
  }
  {
    goPackagePath = "github.com/pmezard/go-difflib";
    fetch = {
      type = "git";
      url = "https://github.com/pmezard/go-difflib";
      rev = "v1.0.0";
      sha256 = "0c1cn55m4rypmscgf0rrb88pn58j3ysvc2d0432dp3c6fqg6cnzw";
    };
  }
  {
    goPackagePath = "github.com/stretchr/objx";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/objx";
      rev = "v0.1.0";
      sha256 = "19ynspzjdynbi85xw06mh8ad5j0qa1vryvxjgvbnyrr8rbm4vd8w";
    };
  }
  {
    goPackagePath = "github.com/stretchr/testify";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/testify";
      rev = "v1.5.1";
      sha256 = "09r89m1wy4cjv2nps1ykp00qjpi0531r07q3s34hr7m6njk4srkl";
    };
  }
  {
    goPackagePath = "golang.org/x/crypto";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/crypto";
      rev = "c2843e01d9a2";
      sha256 = "01xgxbj5r79nmisdvpq48zfy8pzaaj90bn6ngd4nf33j9ar1dp8r";
    };
  }
  {
    goPackagePath = "golang.org/x/net";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/net";
      rev = "16171245cfb2";
      sha256 = "0pb4ap1h28cc4kr59lz94895wvsd2pf1vmd2by6b11ww8mkicn4s";
    };
  }
  {
    goPackagePath = "golang.org/x/sys";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sys";
      rev = "d0b11bdaac8a";
      sha256 = "18yfsmw622l7gc5sqriv5qmck6903vvhivpzp8i3xfy3z33dybdl";
    };
  }
  {
    goPackagePath = "golang.org/x/text";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/text";
      rev = "v0.3.0";
      sha256 = "0r6x6zjzhr8ksqlpiwm5gdd7s209kwk5p4lw54xjvz10cs3qlq19";
    };
  }
  {
    goPackagePath = "gopkg.in/check.v1";
    fetch = {
      type = "git";
      url = "https://gopkg.in/check.v1";
      rev = "20d25e280405";
      sha256 = "0k1m83ji9l1a7ng8a7v40psbymxasmssbrrhpdv2wl4rhs0nc3np";
    };
  }
  {
    goPackagePath = "gopkg.in/yaml.v2";
    fetch = {
      type = "git";
      url = "https://gopkg.in/yaml.v2";
      rev = "v2.2.2";
      sha256 = "01wj12jzsdqlnidpyjssmj0r4yavlqy7dwrg7adqd8dicjc4ncsa";
    };
  }
]
