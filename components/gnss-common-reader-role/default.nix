{ stdenv, lib, awscli2, bash, busybox, jq, makeWrapper }:

let
  runtimeDependencies = [awscli2 bash busybox jq];

in

  stdenv.mkDerivation rec {
    name = "gnss-common-reader-role";
    src = ./.;

    nativeBuildInputs = [
      makeWrapper
    ];

    phases = "installPhase";

    installPhase = ''
      mkdir -p $out/{bin,dist}

      cp -r $src/${name}.sh $out/dist
      chmod +x $out/dist/${name}.sh

      wrapProgram $out/dist/${name}.sh \
        --set PATH /usr/bin:${lib.makeBinPath runtimeDependencies}

      ln -s $out/dist/${name}.sh $out/bin/${name}.sh
    '';
  }
