# GNSS Common Reader Role

This component provides cross-account read-only access to
`gnss-common-nonprod` and `gnss-common-prod` accounts.

```bash
Usage:
  gnss-common-reader-role.sh -e <env>
  eval $(gnss-common-reader-role.sh -e <env> --assume)
options
    -e|--env <env>, where env is 'dev', 'test', or 'prod'
    -a|--assume, instead of printing the role arn, print AWS credential variable assignments
```
