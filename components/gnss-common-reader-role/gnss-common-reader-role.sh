#!/usr/bin/env bash

# This script assumes role gnss-common-$env-reader

set -euo pipefail

scriptName=$(basename "${BASH_SOURCE[0]}")

function usage {
    cat << EOF
Usage:
  $scriptName -e <env>
  eval \$($scriptName -e <env> --assume)
options
    -e|--env <env>, where env is 'dev', 'test', or 'prod'
    -a|--assume, instead of printing the role arn, print AWS credential variable assignments
EOF
}

env=
assume=

while (( $# > 0 )); do
    case $1 in
        -a|--assume)
            assume=true
            shift 1
            ;;
        -e|--env)
            env=${2:-}
            shift 1 && shift 1 || true
            ;;
        *)
            usage
            exit 1
    esac
done

shopt -s extglob
if [[ $env != @(dev|test|prod) ]]; then
    usage
    exit 1
fi

declare -A accountNumber
accountNumber[dev]=023072794443
accountNumber[test]=023072794443
accountNumber[prod]=334594953176

roleArn=arn:aws:iam::${accountNumber[$env]}:role/gnss-common-$env-reader

if [[ -z $assume ]]; then
    echo $roleArn
else
    credentials=$(aws sts assume-role --role-arn "$roleArn" --role-session-name cli | jq '.Credentials')
    echo export AWS_ACCESS_KEY_ID=$(jq <<< "$credentials" '.AccessKeyId' -r)
    echo export AWS_SECRET_ACCESS_KEY=$(jq <<< "$credentials" '.SecretAccessKey' -r)
    echo export AWS_SESSION_TOKEN=$(jq <<< "$credentials" '.SessionToken' -r)
fi
