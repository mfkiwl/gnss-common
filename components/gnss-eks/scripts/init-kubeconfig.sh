#!/usr/bin/env bash

# This script uses `aws eks update-config...` to register cluster gnss-eks-dev with $KUBECONFIG.

set -euo pipefail

function usage {
    cat << EOF
Usage: init-kubeconfig.sh <environment>
where
    environment is dev, test, or prod
EOF
}

while [[ $# -gt 0 ]]; do
    case $1 in
       dev|test|prod)
            environment=$1
            shift
            ;;
        *)
            echo "Unknown option: $1"
            usage
            exit 1
            ;;
    esac
done

if [[ -z ${environment+x} ]]; then
    echo "Unspecified environment"
    usage
    exit 1
fi

export TF_DATA_DIR
TF_DATA_DIR=$(mktemp -d)
trap 'rm -rf "$TF_DATA_DIR"' EXIT

scriptDir="$(dirname "${BASH_SOURCE[0]}")"
cd "$scriptDir"/..

terraformOutputs=$(
    eval "$(gnss-common-reader-role.sh -e "$environment" --assume)"
    terraform init -backend-config "./environments/$environment/backend.cfg" > /dev/null
    terraform workspace select "$environment" > /dev/null
    terraform output -json
)

clusterId=$(jq <<< "$terraformOutputs" -r '.cluster_id.value')
clusterUserRoleArn=$(jq <<< "$terraformOutputs" -r '.cluster_user_role.value')

credentials=$(aws sts assume-role --role-arn "$clusterUserRoleArn" --role-session-name gnss-eks-user | jq '.Credentials')

export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export AWS_SESSION_TOKEN

AWS_ACCESS_KEY_ID=$(jq <<< "$credentials" '.AccessKeyId' -r)
AWS_SECRET_ACCESS_KEY=$(jq <<< "$credentials" '.SecretAccessKey' -r)
AWS_SESSION_TOKEN=$(jq <<< "$credentials" '.SessionToken' -r)

aws eks update-kubeconfig --name "$clusterId" --role-arn "$clusterUserRoleArn"
chmod 600 ~/.kube/config
