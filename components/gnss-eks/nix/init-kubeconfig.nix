{
  stdenv, lib, awscli2, bash, busybox, git, jq, makeWrapper, openssh, terraform_0_13,
  gnssCommonReaderRole
}:

let
  runtimeDependencies = [awscli2 bash busybox git gnssCommonReaderRole jq openssh terraform_0_13];

in

  stdenv.mkDerivation rec {
    name = "init-kubeconfig";
    src = ./..;

    nativeBuildInputs = [
      makeWrapper
    ];

    phases = "installPhase";

    installPhase = ''
      mkdir -p $out/{bin,dist}

      cp -r $src/* $out/dist/
      chmod -R u+w $out/dist/
      chmod +x $out/dist/scripts/${name}.sh

      wrapProgram $out/dist/scripts/${name}.sh \
        --set PATH /usr/bin:${lib.makeBinPath runtimeDependencies}

      ln -s $out/dist/scripts/${name}.sh $out/bin/${name}.sh
    '';
  }
