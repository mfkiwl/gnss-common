locals {
  manifest = "${path.module}/image-pull-secret.yaml"
}

resource "null_resource" "add_image_pull_secret" {
  triggers = {
    manifest    = local.manifest
    config      = filesha1(local.manifest)
    kubeconfig  = filesha1(pathexpand("~/.kube/config"))
  }

  provisioner "local-exec" {
    command = "kubectl apply -f ${local.manifest}"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl delete -f ${self.triggers.manifest}"
  }
}

resource "null_resource" "patch_default_service_account" {
  triggers = {
    kubeconfig  = filesha1(pathexpand("~/.kube/config"))
  }

  provisioner "local-exec" {
    command = "kubectl patch serviceaccount default -p '{\"imagePullSecrets\": [{\"name\": \"image-pull-secret\"}]}'"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl patch serviceaccount default -p '{\"imagePullSecrets\": null}'"
  }

  depends_on = [
    null_resource.add_image_pull_secret,
  ]
}
