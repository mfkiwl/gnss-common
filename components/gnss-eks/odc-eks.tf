locals {
  owner       = "gnss-informatics"
  system      = "gnss-eks"
  release     = "2"

  cluster_id      = "${local.system}-${local.release}-${terraform.workspace}"
  cluster_version = 1.18
}

data "aws_ssm_parameter" "docker_config_json" {
  name = "/dockerhub/user/geodesyarchive/docker-config-json"
}

module "eks" {
  source = "github.com/opendatacube/datacube-k8s-eks//odc_eks?ref=821f57a"

  # Cluster config
  region          = var.region
  cluster_id      = local.cluster_id
  cluster_version = local.cluster_version

  # Default Tags
  owner       = local.owner
  namespace   = local.system
  environment = terraform.workspace

  # VPC config
  vpc_cidr              = "10.49.0.0/16"
  public_subnet_cidrs   = ["10.49.0.0/24", "10.49.1.0/24", "10.49.2.0/24"]
  private_subnet_cidrs  = ["10.49.10.0/24", "10.49.11.0/24", "10.49.12.0/24"]
  database_subnet_cidrs = []

  domain_name = aws_route53_zone.eks.name

  # ACM - used by ALB
  create_certificate = true

  # Nodes
  ami_image_id                 = "ami-024c75fa004fea49d"
  default_worker_instance_type = var.node_instance_type
  spot_nodes_enabled           = false
  min_spot_nodes               = 0
  max_spot_nodes               = 3
  max_spot_price               = "0.40"
  min_nodes                    = 2
  desired_nodes                = 3
  max_nodes                    = 4
  volume_size                  = 100
  spot_volume_size             = 100

  extra_kubelet_args = "--enable-cadvisor-json-endpoints=true"

  extra_userdata = templatefile("${path.module}/node-userdata.sh", {
    docker_config_json = data.aws_ssm_parameter.docker_config_json.value
  })

  # Cloudfront CDN
  cf_enable = false
  # Providing explicit provider for cloudfront distribution certificate - this must be in us-east-1 to work with cloudfront
  # providers = {
  #   aws.us-east-1 = aws.use1
  # }
  # cf_enable                 = true
  # cf_dns_record             = "ows"
  # cf_origin_dns_record      = "cached-alb"
  # cf_custom_aliases         = ["ows.services.${local.domain_name}"]
  # cf_certificate_create     = true
  # cf_origin_protocol_policy = "https-only"
  # cf_log_bucket_create      = false
  # cf_log_bucket             = "${module.odc_cluster_label.id}-cloudfront-logs"

  # WAF
  waf_enable = false
  # waf_enable                                = true
  # waf_target_scope                          = "regional"
  # waf_log_bucket_create                     = false
  # waf_log_bucket                            = "${module.odc_cluster_label.id}-waf-logs"
  # waf_enable_url_whitelist_string_match_set = true
  # waf_url_whitelist_uri_prefix              = "/user"
  # waf_url_whitelist_url_host                = local.sandbox_host_name
}

# TODO: enable later
# data "aws_acm_certificate" "domain_cert" {
#   count       = local.create_certificate ? 0 : 1
#   domain      = "*.${local.domain_name}"
#   most_recent = true
# }
