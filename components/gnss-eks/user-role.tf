data "aws_iam_policy_document" "assume_role" {
  statement {
    sid = "1"

    actions = ["sts:AssumeRole"]

    # List of users
    principals {
      type = "AWS"
      identifiers = var.accounts_iam_root_arns
    }

    # Enforce MFA
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["true"]
    }
  }
}

resource "aws_iam_role" "eks_user" {
  name                 = "user.${local.cluster_id}"
  assume_role_policy   = data.aws_iam_policy_document.assume_role.json
  max_session_duration = "28800"
}


resource "aws_iam_policy" "eks_user" {
  name        = "user-policy.${local.cluster_id}"
  description = "Enables EKS users to get the kubeconfig file using aws cli"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "eks:DescribeCluster"
      ],
      "Effect": "Allow",
      "Resource": "${module.eks.cluster_arn}"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "eks_user" {
  role       = aws_iam_role.eks_user.name
  policy_arn = aws_iam_policy.eks_user.arn
}
