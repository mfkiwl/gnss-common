output "cluster_id" {
  value = module.eks.cluster_id
}

output "cluster_user_role" {
  value = aws_iam_role.eks_user.arn
}
