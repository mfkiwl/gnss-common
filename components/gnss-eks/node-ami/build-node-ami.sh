#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"

rm -rf tenableagentinstall
git clone git@bitbucket.org:geoscienceaustralia/tenableagentinstall.git
cp tenableagentinstall/installtenableagent.sh .
packer build packer.json
