variable "region" {
}

variable "accounts_iam_root_arns" {
  type = list(string)
}

variable "node_instance_type" {
  type = string
  default = "t2.medium"
}
