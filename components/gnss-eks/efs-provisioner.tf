locals {
  application = "efs-provisioner"
  prefix = "${local.cluster_id}-${local.application}"
}

resource "kubernetes_namespace" "efs_provisioner" {
  metadata {
    name = local.application
  }
}

resource "aws_efs_file_system" "storage" {
  tags = {
    Name = local.prefix
  }
}

resource "aws_efs_mount_target" "storage" {
  count = length(module.eks.private_subnets)

  file_system_id = aws_efs_file_system.storage.id

  subnet_id = element(
    tolist(module.eks.private_subnets),
    count.index,
  )

  security_groups = [aws_security_group.efs.id]

  depends_on = [
    module.eks
  ]
}

resource "aws_security_group" "efs" {
  name        = local.prefix
  vpc_id      = module.eks.vpc_id

  ingress {
    from_port       = "2049"
    to_port         = "2049"
    protocol        = "tcp"
    security_groups = [module.eks.node_security_group]
  }
}

data "template_file" "efs_provisioner_config" {
  template = file("${path.module}/efs-provisioner.yaml")

  vars = {
    service_account_arn = aws_iam_role.efs_access.arn
    efs_file_system_id  = aws_efs_file_system.storage.id
    region              = var.region
    environment         = terraform.workspace
    dns_name            = aws_efs_file_system.storage.dns_name
  }
}

resource "kubernetes_secret" "efs_provisioner" {
  metadata {
    name      = "efs-provisioner"
    namespace = kubernetes_namespace.efs_provisioner.metadata[0].name
  }

  data = {
    "values.yaml" = data.template_file.efs_provisioner_config.rendered
  }

  type = "Opaque"
}

data "aws_iam_policy_document" "efs_access_trust_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    principals {
      identifiers = [local.cluster_oidc_provider_arn]
      type        = "Federated"
    }

    condition {
      test     = "StringEquals"
      variable = "${local.cluster_oidc_provider_name}:sub"
      values   = ["system:serviceaccount:${local.application}:${local.application}"]
    }
  }
}

resource "aws_iam_role" "efs_access" {
  name = local.prefix
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.efs_access_trust_policy.json
}

data "aws_iam_policy_document" "efs_access" {
  statement {
    actions = [
      "elasticfilesystem:*",
    ]
    resources = [
      aws_efs_file_system.storage.arn,
    ]
  }
}

resource "aws_iam_role_policy" "efs_access" {
  role   = aws_iam_role.efs_access.id
  policy = data.aws_iam_policy_document.efs_access.json
}
