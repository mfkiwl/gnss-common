module "odc_k8s" {
  source = "github.com/opendatacube/datacube-k8s-eks//odc_k8s?ref=777a4d6"

  region     = var.region
  cluster_id = local.cluster_id

  # Default Tags
  owner       = local.owner
  namespace   = local.system
  environment = terraform.workspace

  # Cluster NodeInstanceRole for worker node groups
  node_roles = {
    "system:node:{{EC2PrivateDNSName}}" = module.eks.node_role_arn
  }

  # Cluster Users Access using aws-auth mapUsers config
  user_roles = {
    "cluster-admin": aws_iam_role.eks_user.arn
  }

  # Database
  store_db_creds = false

  # Setup Flux/FluxCloud
  flux_enabled      = true
  flux_version      = "1.6.0"
  flux_git_repo_url = "ssh://git@bitbucket.org/geoscienceaustralia/gnss-helm-releases.git"
  # Deploy master branch to dev, test branch to test, etc.
  flux_git_branch   = terraform.workspace == "dev" ? "master" : terraform.workspace
  flux_git_path     = "helm-releases/${terraform.workspace}"
  flux_git_label    = local.cluster_id
  # flux/fluxcloud configuration - require to update fluxcloud_slack_url
  # flux_additional_args       = "--k8s-secret-name=flux-git-private-key"
  # flux_additional_args       = "--connect=ws://fluxcloud"
  flux_additional_args       = "--manifest-generation=true"
  flux_helm_operator_version = "1.2.0"
  enabled_helm_versions      = "v3"
  # NOTE: Flux flooding the logs for below external images so remove it from Flux auto-release. We are not auto deploying them anyways.
  # issues: https://github.com/fluxcd/flux/issues/1701, https://github.com/fluxcd/flux/issues/2780
  # flux_registry_exclude_images = "index.docker.io/cilium/cilium,index.docker.io/grafana/loki,index.docker.io/grafana/promtail"

  fluxcloud_enabled = false
  # fluxcloud_enabled         = true
  # fluxcloud_slack_url       = "https://hooks.slack.com/services/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"
  # fluxcloud_slack_channel   = "#ga-wms-updates"
  # fluxcloud_slack_name      = "[${upper(local.cluster_id)}] Flux Deployer"
  # fluxcloud_slack_emoji     = ":aussieparrot:"
  # fluxcloud_github_url      = "https://bitbucket.org/geoscienceaustralia/datakube-apps"
  # fluxcloud_commit_template = "{{ .VCSLink }}/commits/{{ .Commit }}"
  # flux_service_account_arn  = module.svc_role_flux.role_arn

  # Cloudwatch Log Group - for fluentd
  # Note: Creating a Cloudwatch Log Group outside a cluster provisioning so log-group can be retain even we destroy the full cluster infrastructure.
  cloudwatch_logs_enabled = false

  depends_on = [
    null_resource.kubeconfig,
  ]
}
