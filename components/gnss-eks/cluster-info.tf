data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_caller_identity" "current" {}

locals {
  cluster_oidc_provider_url  = data.aws_eks_cluster.cluster.identity[0].oidc[0].issuer
  cluster_oidc_provider_name = replace(local.cluster_oidc_provider_url, "https://", "")
  cluster_oidc_provider_arn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.cluster_oidc_provider_name}"
}
