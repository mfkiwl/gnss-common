data "external" "thumbprint" {
  program = ["${path.module}/oidc-provider-thumbprint.sh", var.region]
}

data "aws_eks_cluster" "cluster" {
  provider = aws.source
  name     = var.cluster_id
}

resource "aws_iam_openid_connect_provider" "identity_provider" {
  provider = aws.target

  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.external.thumbprint.result.thumbprint]
  url             = data.aws_eks_cluster.cluster.identity[0].oidc[0].issuer
}
