#!/usr/bin/env bash
# https://github.com/terraform-providers/terraform-provider-aws/issues/10104

set -euo pipefail

region=$1
host=oidc.eks.$region.amazonaws.com

thumbprint=$(echo | openssl s_client -servername "$host" -showcerts -connect "oidc.eks.$region.amazonaws.com:443" \
  | tac | sed -n '/-----END CERTIFICATE-----/,/-----BEGIN CERTIFICATE-----/p; /-----BEGIN CERTIFICATE-----/q' \
  | tac | openssl x509 -fingerprint -sha1 -noout | sed 's/://g' \
  | awk -F= '{print tolower($2)}')

echo "{\"thumbprint\": \"${thumbprint}\"}"
