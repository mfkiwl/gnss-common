terraform {
  required_version = ">= 0.12.0"

  required_providers {
    aws = ">= 2.55" # latest tested version
  }
}

locals {
  accepter_roles = {
    gnss-rds-dev  = "arn:aws:iam::023072794443:role/gnss-rds-dev-vpc-peering-requester"
    gnss-rds-test = "arn:aws:iam::023072794443:role/gnss-rds-test-vpc-peering-requester"
    gnss-rds-prod = "arn:aws:iam::334594953176:role/gnss-rds-prod-vpc-peering-requester"
  }

  db_instance_exists = {
    gnss-rds-dev  = false
    gnss-rds-test = false
    gnss-rds-prod = false
  }
}

provider "aws" {
  region  = var.region

  assume_role {
    role_arn = local.accepter_roles[var.accepter_system]
  }

  alias = "accepter"
}
