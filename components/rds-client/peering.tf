data "aws_caller_identity" "accepter" {
  provider = aws.accepter
}                                           

data "aws_vpc" "accepter" {
  provider = aws.accepter

  tags = {
    Name = var.accepter_system
  }
}

data "aws_route_tables" "accepter" {
  provider = aws.accepter
  vpc_id   = data.aws_vpc.accepter.id

  tags = {
    Name = "${var.accepter_system}-db"
  }
}

locals {
  accepter_vpc = {
    id              = data.aws_vpc.accepter.id
    cidr_block      = data.aws_vpc.accepter.cidr_block_associations.0.cidr_block
    route_table_ids = sort(data.aws_route_tables.accepter.ids)
  }
}

resource "aws_vpc_peering_connection" "requester" {
  vpc_id        = var.requester_vpc.id
  peer_vpc_id   = local.accepter_vpc.id
  peer_owner_id = data.aws_caller_identity.accepter.account_id
  peer_region   = var.region

  tags = {
    Name = "${var.requester_system}-requester-of-${var.accepter_system}"
  }
}

resource "aws_route" "requester" {
  count = var.requester_vpc.route_table_count

  route_table_id            = var.requester_vpc.route_table_ids[count.index]
  destination_cidr_block    = local.accepter_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.requester.id
}

resource "aws_vpc_peering_connection_accepter" "accepter" {
  provider = aws.accepter

  vpc_peering_connection_id = aws_vpc_peering_connection.requester.id
  auto_accept               = true

  tags = {
    Name = "${var.accepter_system}-accepter-of-${var.requester_system}"
  }
}

resource "aws_route" "accepter" {
  provider = aws.accepter
  count    = length(local.accepter_vpc.route_table_ids)

  route_table_id            = local.accepter_vpc.route_table_ids[count.index]
  destination_cidr_block    = var.requester_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.requester.id
}
