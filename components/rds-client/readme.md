# GNSS RDS Client

Create a VPC peering connection to the private RDS instance managed by
[`gnss-common/components/rds`](../rds)

## Usage

See [`./examples/example_rds_client.tf`](./examples/example_rds_client.tf).

```bash
cd ./examples
terraform init
terraform apply -auto-approve
```

Connect via SSM SessionManager and test the VPC peering.

```bash
nc -zv gnss-rds-dev.cw6nydmsclws.ap-southeast-2.rds.amazonaws.com 5432
```

Note that the above RDS endpoint is correct at the time of writing,
but may vary in the future. Get the latest RDS endpoint.

```bash
terraform output rds_endpoint
```

Don't forget to cleanup when you are finished.

```bash
terraform destroy -auto-approve
```
