data "aws_db_instance" "gnss_rds" {
  provider = aws.accepter
  count    = local.db_instance_exists[var.accepter_system] ? 1 : 0

  db_instance_identifier = var.accepter_system
}

output "rds_endpoint" {
  value = try(data.aws_db_instance.gnss_rds[0].endpoint, null)
}
