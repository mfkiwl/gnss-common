variable "region" {
  type = string
}

variable "accounts_iam_root_arns" {
  type    = list(string)
  default = []
}

variable "requester_system" {
  type = string
}

variable "requester_vpc" {
  type = object({
    id                = string
    cidr_block        = string
    route_table_ids   = list(string)
    route_table_count = number
  })
}

variable "accepter_system" {
  type = string
}

variable "db_instance_exists" {
  type    = bool
  default = true
}
