#!/usr/bin/env bash

# Deploy shared RDS
# Usage: ./deploy.sh components/rds dev

set -euo pipefail

component=$1
env=$2
shift 2

dryRun=false
commonVars=$(pwd)/common.tfvars

positional=()
while [ $# -gt 0 ]; do
    case $1 in
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        * )
            positional+=("$1")
            shift
            ;;
    esac
done
set -- "${positional[@]}"

if [ -f "$component/env.sh" ]; then
    . "$component/env.sh"
fi

cd "$component"

rm -rf plan.tf .terraform

backend=environments/$env/backend.cfg
variables=environments/$env/terraform.tfvars

terraform init -backend-config "$backend"
terraform workspace select "$env" || terraform workspace new "$env"

export TF_VAR_region=$AWS_DEFAULT_REGION

terraform plan -input=false -var-file="$commonVars" -var-file="$variables" -out plan.tf $@

if [[ $dryRun = "false" ]]; then
    terraform apply -auto-approve plan.tf
fi

rm -f plan.tf
