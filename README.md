Components which are shared between GNSS Systems.

Individual components can be infracode, terraform modules, library
code, applications, etc. - they are organized under the components/
directory for the sake of keeping the top level directory clean.

See components/<name>/README.md for component specific information.

##### Deployments

All deployable components currently use deploy.sh (but can be use their
own deploy scripts if necessary). Components deployed this way must
include all variables mentioned in common.tfvar.

Default branch performs dev dry-run deployments for all components (or
dry-run prod deployments for prod only components).

Master branch performs dev deployment and prod dry-run deployment for
all components.

`component.<name>` branches should be used for prod deployments.

##### Exported Commands

This repository uses Nix to package some useful commands. See [./default.nix](./default.nix)
for the list of available packages.

For example, you can set your kubeconfig context to point to gnss-eks-2-dev with

```bash
$ nix run -f https://bitbucket.org/geoscienceaustralia/gnss-common/get/master.tar.gz -c init-kubeconfig.sh dev
```

If you're using `niv` in your project, you can add a dependency to this repository by adding

```nix
"gnss-common": {
    "sha256": "1igvbwp0scjxn08brvx82dmkqwgrq5i6zvdpjh0fxcs21vh72zwy",
    "type": "tarball",
    "url": "https://bitbucket.org/geoscienceaustralia/gnss-common/get/d454bc2773f8.tar.gz"
}
```

to your `./nix/sources.json`, and you can add `init-kubeconfig.sh` to your project's
nix-shell `$PATH` with

```nix
{ sources ? import ./sources.nix }:

import sources.nixpkgs {
    overlays = [
        (pkgs: _: {
            gnss-common-pkgs = import sources.gnss-common {};
        })
    ];
}
```

in your `./nix/default.nix`, and

```nix
{ pkgs ? import ./nix }:

pkgs.mkShell {
    buildInputs = with pkgs; [
        gnss-common-pkgs.initKubeconfig
    ];
}
```

in your `./shell.nix`.
