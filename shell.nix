{
  pkgs ? import ./nix {},
  installDevTools ? true
}:

let 
  projectName = "gnss-common";
  this = import ./default.nix {};

  buildTools = with pkgs; [
    this.assumeRoleMavenArtifactoryWriter
    this.gnssCommonReaderRole
    this.parameters
    awscli2
    curl
    git
    fluxctl
    this.initKubeconfig
    kubernetes-helm
    kubectl
    python3Packages.credstash
    docker
    openssh
    openssl
    packer
    terraform
    jq
  ];

  devTools = with pkgs; [
    # eclipses.eclipse-java
    niv
    vgo2nix
  ];

  env = pkgs.buildEnv {
    name = projectName + "-env";
    paths = buildTools ++ (
      if installDevTools then devTools else []
    );
  };

in
  pkgs.mkShell {
    buildInputs = [
      env
    ];
    shellHook = ''
      export PROJECT_NAME=${projectName}
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"

      terraform-0.13() {
          ${pkgs.terraform_0_13}/bin/terraform $@
      }
      export -f terraform-0.13
    '';
  }
