# List of GNSS account IAM Root ARNs which are used to give cross account access to common resources
accounts_iam_root_arns = [
  "arn:aws:iam::023072794443:root", # gnss-common-nonprod
  "arn:aws:iam::334594953176:root", # gnss-common-prod
  "arn:aws:iam::265943217058:root", # gnss-cors-nonprod
  "arn:aws:iam::308901455865:root", # gnss-cors-prod
  "arn:aws:iam::704132972316:root", # gnss-metadata-nonprod
  "arn:aws:iam::768716959406:root", # gnss-realtime-prod
  "arn:aws:iam::255959771636:root", # gnss-realtime-nonprod
  "arn:aws:iam::484116167126:root", # gnss-metadata-prod
  "arn:aws:iam::688660191997:root", # geodesy-operations
  "arn:aws:iam::623223935732:root", # geodesy-operations-prod
  "arn:aws:iam::094928090547:root"  # egeodesy
]

iam_oidc_providers = [
  "oidc.eks.ap-southeast-2.amazonaws.com/id/131FFE77EE3721BCB81CFBAD9A69F644", # gnss-eks-2-dev
  "oidc.eks.ap-southeast-2.amazonaws.com/id/F7D2C0041F66C989451C7851B2FB7FC4", # gnss-eks-2-test
  "oidc.eks.ap-southeast-2.amazonaws.com/id/BA7EE02FBA25889FE1EB95C62EC1318F", # gnss-eks-2-prod
]
