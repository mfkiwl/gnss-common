#!/usr/bin/env bash

set -euo pipefail

username=${1:-geodesyarchive}

aws ssm get-parameter --name "/dockerhub/user/$username/password" --with-decryption --query Parameter.Value --output text |
    docker login -u $username --password-stdin
