{ pkgs ? import ./nix {} }:
   
rec {
  assumeRoleMavenArtifactoryWriter = pkgs.callPackage ./components/maven-artifactory {};
  gnssCommonReaderRole = pkgs.callPackage ./components/gnss-common-reader-role {};
  initKubeconfig = pkgs.callPackage ./components/gnss-eks/nix/init-kubeconfig.nix { inherit gnssCommonReaderRole; };
  parameters = pkgs.callPackage ./components/parameters {};
}
